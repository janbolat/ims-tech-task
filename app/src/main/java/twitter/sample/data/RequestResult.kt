package twitter.sample.data

import twitter.sample.util.ResourceString

sealed class RequestResult<Z> {

    data class Success<Z>(val data: Z) : RequestResult<Z>()
    data class Error<Z>(val error: ResourceString, val exception: Throwable) : RequestResult<Z>()

    override fun toString(): String {
        return when (this) {
            is Success<*> -> "Success[data=$data]"
            is Error -> "Error[error=$error, exception=$exception]"
        }
    }
}

val RequestResult<*>.succeeded
    get() = this is RequestResult.Success && data != null
