package twitter.sample.data.response

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName


/**
 * Created by Zhanbolat Raimbekov on 7/29/21.
 */
@Keep
data class UserResponse(
    val name: String,
    @SerializedName("profile_image_url_https")
    val imageUrl: String
)