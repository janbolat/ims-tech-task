package twitter.sample.data.response

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName


/**
 * Created by Zhanbolat Raimbekov on 7/29/21.
 */
@Keep
data class AuthResponse(
    @SerializedName("token_type")
    val tokenType: String,
    @SerializedName("access_token")
    val accessToken: String
)