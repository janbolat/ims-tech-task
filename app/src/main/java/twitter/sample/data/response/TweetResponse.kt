package twitter.sample.data.response

import androidx.annotation.Keep


/**
 * Created by Zhanbolat Raimbekov on 7/29/21.
 */
@Keep
data class TweetResponse(
    val text: String,
    val user: UserResponse
)