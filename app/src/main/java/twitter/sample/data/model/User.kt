package twitter.sample.data.model


/**
 * Created by Zhanbolat Raimbekov on 7/29/21.
 */
data class User(
    val name: String,
    val imageUrl: String
)