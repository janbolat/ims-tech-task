package twitter.sample.data.model

data class OAuthToken(
    val tokenType: String,
    val accessToken: String
)