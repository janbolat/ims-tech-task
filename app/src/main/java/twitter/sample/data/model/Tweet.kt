package twitter.sample.data.model


/**
 * Created by Zhanbolat Raimbekov on 7/29/21.
 */
data class Tweet(
    val text: String,
    val user: User
)