package twitter.sample.data.model

import twitter.sample.data.response.TweetResponse

object TweetConverter {
    fun fromNetwork(item: TweetResponse) =
        Tweet(
            text = item.text,
            user = User(
                name = item.user.name,
                imageUrl = item.user.imageUrl
            )
        )
}