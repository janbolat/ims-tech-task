package twitter.sample.util

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import timber.log.Timber

fun CoroutineScope.launchSafe(
    body: suspend () -> Unit,
    onError: ((e: Throwable) -> Unit)? = null,
    onStart: (() -> Unit)? = null,
    onFinish: (() -> Unit)? = null
): Job =
    launch {
        try {
            onStart?.invoke()
            body.invoke()
        } catch (e: Throwable) {
            Timber.e(e)
            onError?.invoke(e)
        } finally {
            onFinish?.invoke()
        }
    }