package twitter.sample.util

import android.content.Context

sealed class ResourceString {
    abstract fun format(context: Context): String
}
class IdResourceString(val id: Int): ResourceString() {
    override fun format(context: Context): String = context.getString(id)
}
class TextResourceString(val text: String): ResourceString() {
    override fun format(context: Context): String = text
}
class FormatResourceString(val id: Int, vararg val args: Any): ResourceString() {
    override fun format(context: Context): String = context.getString(id, *args)
}