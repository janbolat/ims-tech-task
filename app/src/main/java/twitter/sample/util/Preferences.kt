package twitter.sample.util

import android.content.SharedPreferences
import androidx.core.content.edit

private const val KEY_ACCESS_TOKEN = "KEY_ACCESS_TOKEN"

class Preferences(private val preferences: SharedPreferences) {

    fun saveAccessToken(accessToken: String) {
        preferences.edit {
            putString(KEY_ACCESS_TOKEN, accessToken)
        }
    }

    fun getAccessToken(): String =
        preferences.getString(KEY_ACCESS_TOKEN, "").orEmpty()
}