package twitter.sample.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import twitter.sample.R
import twitter.sample.data.RequestResult
import twitter.sample.data.model.Tweet
import twitter.sample.domain.interactor.GetTweetsUseCase
import twitter.sample.domain.interactor.RefreshTokenUseCase
import twitter.sample.util.*

/**
 * Created by Zhanbolat Raimbekov on 7/29/21.
 */
class TwitterViewModel(
    private val getTweetsUseCase: GetTweetsUseCase,
    private val refreshTokenUseCase: RefreshTokenUseCase,
    private val preferences: Preferences
) : BaseViewModel() {

    private val _getTweetsActions = MutableLiveData<GetTweetsAction>()
    val getTweetsActions: LiveData<GetTweetsAction> = _getTweetsActions

    fun getTweets(username: String) {
        launchSafe(
            body = {
                getToken() ?: return@launchSafe

                when (val result = getTweetsUseCase(GetTweetsUseCase.Param(username = username, count = 1))) {
                    is RequestResult.Success ->
                        _getTweetsActions.postValue(GetTweetsAction.Success(result.data))
                    is RequestResult.Error ->
                        _getTweetsActions.postValue(GetTweetsAction.ShowError(result.error))
                }
            },
            onStart = { _getTweetsActions.value = GetTweetsAction.ShowLoading(true) },
            onFinish = { _getTweetsActions.value = GetTweetsAction.ShowLoading(false) }
        )
    }

    private suspend fun getToken(): String? =
        when (val authResult = refreshTokenUseCase(RefreshTokenUseCase.Param("client_credentials"))) {
            is RequestResult.Success -> {
                val token = authResult.data.accessToken
                preferences.saveAccessToken(token)
                token
            }
            else -> {
                _getTweetsActions.postValue(GetTweetsAction.ShowError(IdResourceString(R.string.failed_to_get_token)))
                null
            }
        }
}

sealed class GetTweetsAction {
    data class Success(val tweets: List<Tweet>) : GetTweetsAction()
    data class ShowError(val resource: ResourceString) : GetTweetsAction()
    data class ShowLoading(val isLoading: Boolean) : GetTweetsAction()
}
