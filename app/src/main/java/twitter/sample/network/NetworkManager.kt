package twitter.sample.network

import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build

class NetworkManager(
    private val connectivityManager: ConnectivityManager
) {

    @Suppress("DEPRECATION")
    fun isConnectedToNetwork(): Boolean = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)?.let { actNw ->
            actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) ||
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) ||
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) ||
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_BLUETOOTH) ||
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_VPN)
        } ?: false
    } else {
        connectivityManager.activeNetworkInfo?.isConnected ?: false
    }

    fun throwIfNoConnection() {
        if (!isConnectedToNetwork()) throw NoNetworkConnectionException
    }
}