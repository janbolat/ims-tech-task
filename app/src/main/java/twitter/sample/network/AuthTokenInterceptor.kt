package twitter.sample.network

import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import twitter.sample.util.Preferences


class AuthTokenInterceptor(private val preferences: Preferences) : Interceptor {

    companion object {
        const val HEADER_AUTHORIZATION_KEY = "Authorization"
        const val HEADER_AUTHORIZATION_BEARER = "Bearer"

        fun attachToken(accessToken: String, request: Request): Request =
            if (accessToken.isNotEmpty()) {
                request.newBuilder()
                    .header(HEADER_AUTHORIZATION_KEY, "$HEADER_AUTHORIZATION_BEARER $accessToken")
                    .build()
            } else {
                request
            }
    }

    override fun intercept(chain: Interceptor.Chain): Response =
        chain.proceed(attachToken(preferences.getAccessToken(), chain.request()))
}
