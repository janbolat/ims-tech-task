package twitter.sample.remote

import twitter.sample.data.model.Tweet
import twitter.sample.data.model.TweetConverter
import twitter.sample.domain.interactor.TwitterRemoteGateway
import twitter.sample.network.NetworkManager


/**
 * Created by Zhanbolat Raimbekov on 7/29/21.
 */
class TwitterApi(
    private val service: TwitterService,
    private val networkManager: NetworkManager
) : TwitterRemoteGateway {

    override suspend fun getLatestTweets(username: String, count: Int): List<Tweet> {
        networkManager.throwIfNoConnection()
        return service.getLatestTweets(username, count).map {
            TweetConverter.fromNetwork(it)
        }
    }
}