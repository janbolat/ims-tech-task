package twitter.sample.remote

import twitter.sample.data.model.OAuthToken
import twitter.sample.domain.interactor.TwitterAuthRemoteGateway
import twitter.sample.network.NetworkManager


/**
 * Created by Zhanbolat Raimbekov on 7/29/21.
 */
class TwitterAuthApi(
    private val service: TwitterAuthService,
    private val networkManager: NetworkManager
) : TwitterAuthRemoteGateway {

    override suspend fun getCredentials(grantType: String): OAuthToken {
        networkManager.throwIfNoConnection()
        val credentials = service.getCredentials(grantType)
        return OAuthToken(tokenType = credentials.tokenType, accessToken = credentials.accessToken)
    }
}