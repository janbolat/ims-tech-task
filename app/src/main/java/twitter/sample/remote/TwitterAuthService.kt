package twitter.sample.remote

import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST
import twitter.sample.data.response.AuthResponse


interface TwitterAuthService {
    @FormUrlEncoded
    @POST("oauth2/token")
    suspend fun getCredentials(@Field("grant_type") grantType: String): AuthResponse
}