package twitter.sample.remote

import retrofit2.http.*
import twitter.sample.data.response.TweetResponse


interface TwitterService {
    @GET("1.1/statuses/user_timeline.json")
    suspend fun getLatestTweets(
        @Query("screen_name") username: String,
        @Query("count") count: Int,
    ): List<TweetResponse>
}