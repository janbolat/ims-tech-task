package twitter.sample.ui

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_second.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import twitter.sample.R
import twitter.sample.viewmodels.GetTweetsAction
import twitter.sample.viewmodels.TwitterViewModel

class SecondFragment : Fragment(R.layout.fragment_second) {

    private val args: SecondFragmentArgs by navArgs()
    private val viewModel: TwitterViewModel by viewModel()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toolbar.setNavigationOnClickListener { findNavController().navigateUp() }
        observeData()
        if (savedInstanceState == null) {
            viewModel.getTweets(args.username)
        }
    }

    private fun observeData() {
        viewModel.getTweetsActions.observe(viewLifecycleOwner) {
            when (it) {
                is GetTweetsAction.ShowLoading ->
                    loadingProgressBar.isVisible = it.isLoading
                is GetTweetsAction.ShowError -> {
                    errorTextView.isVisible = true
                    errorTextView.text = it.resource.format(requireContext())
                }
                is GetTweetsAction.Success ->
                    it.tweets.firstOrNull()?.let { tweet ->
                        tweetTextView.isVisible = true
                        tweetTextView.text = tweet.text
                        imageView.isVisible = true
                        Glide.with(imageView)
                            .load(tweet.user.imageUrl)
                            .into(imageView)
                    }
            }
        }
    }
}