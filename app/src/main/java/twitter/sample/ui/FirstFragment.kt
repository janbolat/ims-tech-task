package twitter.sample.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.fragment_first.*
import twitter.sample.R

class FirstFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View =
        inflater.inflate(R.layout.fragment_first, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        usernameTextInputEditText.requestFocus()

        fetchButton.setOnClickListener {
            val username = usernameTextInputEditText.text.toString()
            if (username.isNotEmpty()) {
                usernameTextInputLayout.error = null
                findNavController().navigate(FirstFragmentDirections.actionFirstFragmentToSecondFragment(username))
            } else {
                usernameTextInputLayout.error =
                    view.context.getString(R.string.please_enter_username)
            }
        }
    }
}