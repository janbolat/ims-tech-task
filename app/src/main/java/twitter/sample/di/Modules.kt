package twitter.sample.di

import org.koin.core.module.Module


/**
 * Created by Zhanbolat Raimbekov on 7/29/21.
 */
object Modules {
    val modules: List<Module> =
        listOf(
            CommonModule.create(),
            NetworkModule.create(),
            TwitterModule.create(),
        )
}