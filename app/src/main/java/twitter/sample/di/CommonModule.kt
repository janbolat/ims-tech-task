package twitter.sample.di

import android.content.Context
import android.content.SharedPreferences
import org.koin.core.module.Module
import org.koin.dsl.bind
import org.koin.dsl.module
import twitter.sample.util.Preferences

/**
 * Created by Zhanbolat Raimbekov on 7/29/21.
 */
object CommonModule : InjectionModule {

    override fun create(): Module = module {
        single {
            val context: Context = get()
            return@single context.getSharedPreferences("twitter", Context.MODE_PRIVATE)
        } bind SharedPreferences::class

        single { Preferences(get()) }
    }
}