package twitter.sample.di

import okhttp3.Credentials
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.bind
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import twitter.sample.domain.interactor.GetTweetsUseCase
import twitter.sample.domain.interactor.RefreshTokenUseCase
import twitter.sample.domain.interactor.TwitterAuthRemoteGateway
import twitter.sample.domain.interactor.TwitterRemoteGateway
import twitter.sample.network.AuthTokenInterceptor
import twitter.sample.remote.TwitterApi
import twitter.sample.remote.TwitterAuthApi
import twitter.sample.remote.TwitterAuthService
import twitter.sample.remote.TwitterService
import twitter.sample.viewmodels.TwitterViewModel


/**
 * Created by Zhanbolat Raimbekov on 7/29/21.
 */
object TwitterModule : InjectionModule {

    override fun create(): Module = module {

        // auth
        single<TwitterAuthRemoteGateway> { TwitterAuthApi(get(), get()) }

        factory { RefreshTokenUseCase(get()) }

        single<TwitterAuthService> {
            Retrofit.Builder()
                .baseUrl("https://api.twitter.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(
                    get<OkHttpClient.Builder>()
                        .addInterceptor(object : Interceptor {
                            override fun intercept(chain: Interceptor.Chain): Response {
                                val request = chain.request()
                                return chain.proceed(
                                    request.newBuilder()
                                        .header(
                                            name = AuthTokenInterceptor.HEADER_AUTHORIZATION_KEY,
                                            value = Credentials.basic("05PdBFkkHWNfLyN1uSRRGNODM", "26VZlMUzG22Qdvw8Fqz0slNsEeddymgZiz7P9I8eZPjEExZFR9")
                                        )
                                        .build()
                                )
                            }
                        })
                        .build()
                )
                .build()
                .create(TwitterAuthService::class.java)
        } bind TwitterAuthService::class


        // twitter
        single<TwitterRemoteGateway> { TwitterApi(get(), get()) }

        single<TwitterService> {
            Retrofit.Builder()
                .baseUrl("https://api.twitter.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(
                    get<OkHttpClient.Builder>()
                        .addInterceptor(AuthTokenInterceptor(get()))
                        .build()
                )
                .build()
                .create(TwitterService::class.java)
        } bind TwitterService::class

        viewModel { TwitterViewModel(get(), get(), get()) }

        factory { GetTweetsUseCase(get()) }
    }
}