package twitter.sample.di

import android.content.Context
import android.net.ConnectivityManager
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.core.module.Module
import org.koin.dsl.module
import twitter.sample.BuildConfig
import twitter.sample.network.NetworkManager
import java.util.concurrent.TimeUnit


object NetworkModule : InjectionModule {

    private const val NETWORK_TIMEOUT = 10L

    override fun create(): Module = module {
        factory {
            OkHttpClient.Builder()
                .readTimeout(NETWORK_TIMEOUT, TimeUnit.SECONDS)
                .connectTimeout(NETWORK_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(NETWORK_TIMEOUT, TimeUnit.SECONDS)
                .addInterceptor(HttpLoggingInterceptor().apply {
                    level = if (BuildConfig.DEBUG) {
                        HttpLoggingInterceptor.Level.BODY
                    } else {
                        HttpLoggingInterceptor.Level.NONE
                    }
                })
        }

        single {
            NetworkManager(
                get<Context>().getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            )
        }
    }
}