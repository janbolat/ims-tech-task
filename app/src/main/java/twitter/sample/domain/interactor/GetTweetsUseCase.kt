package twitter.sample.domain.interactor

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import twitter.sample.data.model.Tweet

class GetTweetsUseCase(
    private val repository: TwitterRemoteGateway
) : RequestUseCase<GetTweetsUseCase.Param, List<Tweet>>() {

    override val dispatcher: CoroutineDispatcher = Dispatchers.IO

    override suspend fun execute(params: Param): List<Tweet> =
        repository.getLatestTweets(params.username, params.count)

    data class Param(val username: String, val count: Int)
}