package twitter.sample.domain.interactor

import twitter.sample.data.model.OAuthToken

/**
 * Created by Zhanbolat Raimbekov on 7/29/21.
 */
interface TwitterAuthRemoteGateway {

    suspend fun getCredentials(grantType: String): OAuthToken
}