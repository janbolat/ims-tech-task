package twitter.sample.domain.interactor

import com.google.gson.JsonSyntaxException
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import twitter.sample.R
import twitter.sample.data.RequestResult
import twitter.sample.network.NoNetworkConnectionException
import twitter.sample.util.FormatResourceString
import twitter.sample.util.IdResourceString
import twitter.sample.util.TextResourceString
import java.net.ConnectException
import java.net.HttpURLConnection.*
import java.net.SocketTimeoutException

abstract class RequestUseCase<P, T> : UseCase<P, T> {

    open suspend operator fun invoke(params: P): RequestResult<T> =
        withContext(dispatcher) {
            return@withContext try {
                RequestResult.Success(execute(params)) as RequestResult<T>
            } catch (e: Exception) {
                handleException(e)
            }
        }

    private fun handleException(e: Exception): RequestResult<T> =
        when (e) {
            is NoNetworkConnectionException ->
                RequestResult.Error(IdResourceString(R.string.request_no_connection_error), e)
            is ConnectException ->
                RequestResult.Error(IdResourceString(R.string.request_connection_error), e)
            is SocketTimeoutException ->
                RequestResult.Error(IdResourceString(R.string.request_timeout_error), e)
            is HttpException -> when (e.code()) {
                HTTP_BAD_REQUEST ->
                    RequestResult.Error(IdResourceString(R.string.request_processing_error), e)
                HTTP_UNAUTHORIZED ->
                    RequestResult.Error(IdResourceString(R.string.request_http_error_401), e)
                HTTP_FORBIDDEN ->
                    RequestResult.Error(IdResourceString(R.string.request_http_error_403), e)
                HTTP_NOT_FOUND ->
                    RequestResult.Error(IdResourceString(R.string.request_http_error_404), e)
                HTTP_INTERNAL_ERROR ->
                    RequestResult.Error(IdResourceString(R.string.request_http_error_500), e)
                else -> {
                    val errorBody = e.response()?.errorBody()?.string()
                    RequestResult.Error(
                        if (!errorBody.isNullOrBlank()) {
                            TextResourceString(errorBody)
                        } else {
                            FormatResourceString(R.string.request_http_error_format, e.code())
                        },
                        e
                    )
                }
            }
            is JsonSyntaxException ->
                RequestResult.Error(IdResourceString(R.string.request_processing_error), e)
            else ->
                RequestResult.Error(
                    FormatResourceString(
                        R.string.request_error,
                        e::class.java.simpleName,
                        e.localizedMessage.orEmpty()
                    ),
                    e
                )
        }
}