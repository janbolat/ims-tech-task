package twitter.sample.domain.interactor

import kotlinx.coroutines.CoroutineDispatcher

interface UseCase<in P, T> {

    val dispatcher: CoroutineDispatcher

    suspend fun execute(params: P): T
}