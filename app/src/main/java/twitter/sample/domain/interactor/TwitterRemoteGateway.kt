package twitter.sample.domain.interactor

import twitter.sample.data.model.Tweet

/**
 * Created by Zhanbolat Raimbekov on 7/29/21.
 */
interface TwitterRemoteGateway {

    suspend fun getLatestTweets(username: String, count: Int): List<Tweet>
}