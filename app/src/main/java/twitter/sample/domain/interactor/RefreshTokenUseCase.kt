package twitter.sample.domain.interactor

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import twitter.sample.data.model.OAuthToken

class RefreshTokenUseCase(
    private val repository: TwitterAuthRemoteGateway
) : RequestUseCase<RefreshTokenUseCase.Param, OAuthToken>() {

    override val dispatcher: CoroutineDispatcher = Dispatchers.IO

    override suspend fun execute(params: Param): OAuthToken =
        repository.getCredentials(params.grantType)

    data class Param(val grantType: String)
}